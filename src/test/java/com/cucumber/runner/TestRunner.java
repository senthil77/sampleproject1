package com.cucumber.runner;

import javax.sql.rowset.WebRowSet;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;



@RunWith(Cucumber.class)
@CucumberOptions(
		features="src\\test\\java\\com\\cucumber\\feature",
		glue="com\\cucumber\\stepdefinition"
		plugin= {"com.cucumber.listener.ExtentCucumberFormatter:Report/report.html"}
		)



public class TestRunner {
	public static WebDriver driver;
	@BeforeClass
	public static void browserLaunch() {
		driver=BaseClass.browserLaunch("chrome");
		
	}
	@AfterClass
	public static void teardown() {
		driver.close();
	}
	

}
